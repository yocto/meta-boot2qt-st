SRC_URI += "file://kms.conf"

QT_QPA_PLATFORM:stm32mp13common = "linuxfb"

do_configure:append() {
    echo "FB_MULTI_BUFFER=2" >> ${WORKDIR}/defaults
    echo "QT_QPA_EGLFS_FORCE888=1" >> ${WORKDIR}/defaults
    echo "QT_QPA_EGLFS_FORCEVSYNC=1" >> ${WORKDIR}/defaults
    echo "QT_QPA_EGLFS_KMS_ATOMIC=1" >> ${WORKDIR}/defaults
    echo "QT_QPA_EGLFS_KMS_CONFIG=/etc/kms.conf" >> ${WORKDIR}/defaults
}

do_configure:append:stm32mp13common() {
    echo "QSG_NO_DEPTH_BUFFER=1" ${WORKDIR}/defaults
    echo "QSG_NO_STENCIL_BUFFER=1" ${WORKDIR}/defaults
    echo "QSG_NO_CLEAR_BUFFERS=1" ${WORKDIR}/defaults
    echo "QT_QUICK_BACKEND=software" ${WORKDIR}/defaults
    echo "QT_QPA_FB_DRM=1" >> ${WORKDIR}/defaults
    echo "QT_QUICK_BACKEND=software" >> ${WORKDIR}/defaults
    echo "QSG_RENDER_LOOP=basic" >> ${WORKDIR}/defaults
    echo "QMLSCENE_DEVICE=softwarecontext" >> ${WORKDIR}/defaults
}

do_install:append() {
    install -m 0644 ${WORKDIR}/kms.conf ${D}${sysconfdir}/
}
