############################################################################
##
## Copyright (C) 2021 The Qt Company Ltd.
## Contact: https://www.qt.io/licensing/
##
## This file is part of the Boot to Qt meta layer.
##
## $QT_BEGIN_LICENSE:GPL$
## Commercial License Usage
## Licensees holding valid commercial Qt licenses may use this file in
## accordance with the commercial license agreement provided with the
## Software or, alternatively, in accordance with the terms contained in
## a written agreement between you and The Qt Company. For licensing terms
## and conditions see https://www.qt.io/terms-conditions. For further
## information use the contact form at https://www.qt.io/contact-us.
##
## GNU General Public License Usage
## Alternatively, this file may be used under the terms of the GNU
## General Public License version 3 or (at your option) any later version
## approved by the KDE Free Qt Foundation. The licenses are as published by
## the Free Software Foundation and appearing in the file LICENSE.GPL3
## included in the packaging of this file. Please review the following
## information to ensure the GNU General Public License requirements will
## be met: https://www.gnu.org/licenses/gpl-3.0.html.
##
## $QT_END_LICENSE$
##
############################################################################

PACKAGECONFIG += "gbm kms"
PACKAGECONFIG:remove:stm32mp13common = "vulkan"
PACKAGECONFIG:append:stm32mp13common = " linuxfb"

QT_QPA_EGLFS_INTEGRATION ?= "eglfs_kms"

# Default platform plugin for MP135
QT_QPA_DEFAULT_PLATFORM:stm32mp13common = "linuxfb"

# Add GIF support
QT_CONFIG_FLAGS += " -gif"

# Remove GLESv3 support
QT_CONFIG_FLAGS += " -no-sse2 -no-opengles3"

# Add LinuxFB support for STM32MP135
QT_CONFIG_FLAGS:append:stm32mp13common = " -linuxfb"
