# Override libegl, libglesv1, libglesv2 and libopenvg package content so that
# libEGL.so, libGLESv1_CM.so, libGLESv2.so and libOpenVG.so are available
# at EGL context creation in case all GPU stack is installed in /usr/lib
FILES:libegl-gcnano = "${GCNANO_USERLAND_OUTPUT_LIBDIR}/libEGL.so*"
FILES:libgles1-gcnano = "${GCNANO_USERLAND_OUTPUT_LIBDIR}/libGLESv1_CM.so*"
FILES:libgles2-gcnano = "${GCNANO_USERLAND_OUTPUT_LIBDIR}/libGLESv2.so* ${GCNANO_USERLAND_OUTPUT_LIBDIR}/libGLSLC.so"
FILES:libopenvg-gcnano = "${GCNANO_USERLAND_OUTPUT_LIBDIR}/libOpenVG*.so*"
