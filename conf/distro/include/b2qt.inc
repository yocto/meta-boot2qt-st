############################################################################
##
## Copyright (C) 2021 The Qt Company Ltd.
## Contact: https://www.qt.io/licensing/
##
## This file is part of the Boot to Qt meta layer.
##
## $QT_BEGIN_LICENSE:GPL$
## Commercial License Usage
## Licensees holding valid commercial Qt licenses may use this file in
## accordance with the commercial license agreement provided with the
## Software or, alternatively, in accordance with the terms contained in
## a written agreement between you and The Qt Company. For licensing terms
## and conditions see https://www.qt.io/terms-conditions. For further
## information use the contact form at https://www.qt.io/contact-us.
##
## GNU General Public License Usage
## Alternatively, this file may be used under the terms of the GNU
## General Public License version 3 or (at your option) any later version
## approved by the KDE Free Qt Foundation. The licenses are as published by
## the Free Software Foundation and appearing in the file LICENSE.GPL3
## included in the packaging of this file. Please review the following
## information to ensure the GNU General Public License requirements will
## be met: https://www.gnu.org/licenses/gpl-3.0.html.
##
## $QT_END_LICENSE$
##
############################################################################

DEPLOY_CONF_NAME:stm32mp15-eval  = "STM32MP15 Evaluation"
DEPLOY_CONF_NAME:stm32mp15-disco = "STM32MP15 Discovery"
DEPLOY_CONF_NAME:stm32mp13-disco = "STM32MP13 Discovery"

ACCEPT_EULA_stm32mp15-eval   = "1"
ACCEPT_EULA_stm32mp15-disco  = "1"
ACCEPT_EULA_stm32mp13-disco  = "1"

DISTRO_FEATURES:remove = "webengine"

IMAGE_FSTYPES:remove = "tar.xz"

WKS_IMAGE_FSTYPES += "wic wic.xz wic.bmap conf"
OPTEE_WIC_FILE ?= ""
TRUSTED_WIC_FILE ?= ""
WKS_FILE += "${@bb.utils.contains('BOOTSCHEME_LABELS', 'optee', '${OPTEE_WIC_FILE}', '${TRUSTED_WIC_FILE}', d)}"

DEPLOY_CONF_IMAGE_TYPE = "wic.xz"
QBSP_IMAGE_CONTENT += "\
    ${IMAGE_LINK_NAME}.${DEPLOY_CONF_IMAGE_TYPE} \
    ${IMAGE_LINK_NAME}.conf \
    ${IMAGE_LINK_NAME}.info \
    "

# Define ROOTFS_MAXSIZE to 3GB
IMAGE_ROOTFS_MAXSIZE = "3145728"

# ogl-runtime depends on gles3
OGL_RUNTIME = ""
OGL_RUNTIME_DEV = ""

ST_VENDORFS = "0"

TOOLCHAIN_HOST_TASK:remove = "${ST_TOOLS_FOR_SDK}"
TOOLCHAIN_HOST_TASK:remove = "${ST_DEPENDENCIES_BUILD_FOR_SDK}"

IMAGE_PREPROCESS_COMMAND:remove = "extract_buildinfo;"
